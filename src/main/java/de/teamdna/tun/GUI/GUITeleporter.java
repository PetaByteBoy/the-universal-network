package de.teamdna.tun.GUI;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

import de.teamdna.tun.TUN;
import de.teamdna.tun.GUI.Element.GuiTUNButton;
import de.teamdna.tun.Net.ClientProxy;
import de.teamdna.tun.Net.Packet.PacketTeleportRequest;
import de.teamdna.tun.Net.Packet.PacketTeleporterSync;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.TileEntity.TileEntityTeleporter;
import de.teamdna.tun.Util.Reference;
import de.teamdna.tun.Util.WorldBlock;

public class GUITeleporter extends GuiContainer {

	private static final ResourceLocation background = new ResourceLocation(Reference.modid, "textures/gui/container/teleporter_gui.png");
	
	public WorldBlock block;
	public EntityPlayer player;
	
	private GuiTextField textField;
	private List<TileEntity> teleporter;
	
	public GUITeleporter(World world, int x, int y, int z, EntityPlayer player) {
		super(new ContainerTeleporter(world, x, y, z, player));
		this.block = new WorldBlock(world, x, y, z);
		this.block.updateWorld();
		this.player = player;
		
		this.xSize = 200;
		this.ySize = 200;
	}
	
	@Override
	public void initGui() {
        super.initGui();
        Keyboard.enableRepeatEvents(true);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.textField = new GuiTextField(this.fontRendererObj, i + this.xSize / 2 - 113 / 2, j + 46, 113, 12);
        this.textField.setTextColor(-1);
        this.textField.setDisabledTextColour(-1);
        this.textField.setMaxStringLength(12);
        this.textField.setEnabled(true);
        this.textField.setText(((TileEntityTeleporter)this.block.updateTileEntity()).name);
        
        this.teleporter = ((TileEntityNetworkManager)(new WorldBlock(((INetworkComponent)this.block.tile).getLink())).tile).getNetworkTilesOfType(TileEntityTeleporter.class);
        this.teleporter.remove(this.block.tile);
        Collections.sort(this.teleporter, new Comparator<TileEntity>() {
			public int compare(TileEntity o1, TileEntity o2) {
				return ((TileEntityTeleporter)o1).name.toLowerCase().compareTo(((TileEntityTeleporter)o2).name.toLowerCase());
			}
		});
        
        for(int i2 = 0; i2 < this.teleporter.size(); i2++) {
			if(i2 >= 16) break;
			
			int col = i2 >= 8 ? 1 : 0;
			int x = i + this.xSize / 2 - 35 - 40 + 80 * col + (this.teleporter.size() <= 8 ? 40 : 0);
			int y = j + 65 + (i2 - 8 * col) * 16;
			int width = 70;
			int height = 14;
			this.buttonList.add(new GuiTUNButton(i2, x, y, width, height, ((TileEntityTeleporter)this.teleporter.get(i2)).name));
		}
    }
	
	@Override
	public void onGuiClosed() {
        super.onGuiClosed();
        Keyboard.enableRepeatEvents(false);
        
        TileEntityTeleporter tile = (TileEntityTeleporter)this.block.updateTileEntity();
        
        if(!tile.name.equals(this.textField.getText().trim())) {
        	tile.name = this.textField.getText().trim();
        	tile.customName = true;
            TUN.packetPipeline.sendToServer(new PacketTeleporterSync(this.block.world, this.block.x, this.block.y, this.block.z, this.textField.getText()));
        }
    }

	@Override
	protected void drawGuiContainerForegroundLayer(int par1, int par2) {
		if(this.teleporter.size() == 0) {
			String s = "No teleporters in network";
			ClientProxy.customFont1.drawString(this.xSize / 2 + ClientProxy.customFont1.getStringWidth(s) / 2 * 0.2F - 50, 600, s, Color.white, 0.2F);
		}
		
		ClientProxy.customFont1.drawString(104, 34, "TUN Teleporter", Color.white, 0.3F);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
	   	int k = (this.width - this.xSize) / 2;
	   	int l = (this.height - this.ySize) / 2;

		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glEnable(GL11.GL_BLEND);
		
		GL11.glTranslatef(k, l, 0);
		this.mc.getTextureManager().bindTexture(background);

		float f = 0.00390625F / 3.91F;
        float f1 = 0.00390625F / 3.125F;
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(0, 0, this.zLevel, 0, 0);
        tessellator.addVertexWithUV(0, this.ySize, this.zLevel, 0, 800 * f1);
        tessellator.addVertexWithUV(this.xSize, this.ySize, this.zLevel, 1000 * f, 800 * f1);
        tessellator.addVertexWithUV(this.xSize, 0, this.zLevel, 1000 * f, 0);
        tessellator.draw();
		
		GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
	}
	
	@Override
	public void drawScreen(int par1, int par2, float par3) {
        super.drawScreen(par1, par2, par3);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_BLEND);
        this.textField.drawTextBox();
    }
	
	@Override
	protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        this.textField.mouseClicked(par1, par2, par3);
    }
	
	@Override
	protected void keyTyped(char par1, int par2) {
        if(!this.textField.textboxKeyTyped(par1, par2)) {
        	super.keyTyped(par1, par2);
        }
    }
	
	@Override
	protected void actionPerformed(GuiButton button) {
		TileEntityTeleporter tile = (TileEntityTeleporter)this.teleporter.get(button.id);
		TUN.packetPipeline.sendToServer(new PacketTeleportRequest(this.mc.thePlayer.getEntityId(), this.mc.thePlayer.worldObj.provider.dimensionId, (new WorldBlock(tile)).getBlockUID(), ((INetworkComponent)this.block.tile).getLink()));
		Minecraft.getMinecraft().thePlayer.closeScreen();
	}
	
}
