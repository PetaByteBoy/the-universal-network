package de.teamdna.tun.GUI.Element;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

import de.teamdna.tun.Net.ClientProxy;
import de.teamdna.tun.Util.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.MouseHelper;
import net.minecraft.util.ResourceLocation;

public class GuiTUNButton extends GuiButton {

	private final ResourceLocation button = new ResourceLocation(Reference.modid, "textures/gui/gui_button.png");
	private final ResourceLocation button_hover = new ResourceLocation(Reference.modid, "textures/gui/gui_button_hover.png");
	
	public GuiTUNButton(int id, int x, int y, int width, int height, String text) {
		super(id, x, y, width, height, text);
	}

	@Override
	public void drawButton(Minecraft mc, int mx, int my)
    {
        if (this.visible)
        {
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glEnable(GL11.GL_BLEND);
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            
            this.field_146123_n = mx >= this.xPosition && my >= this.yPosition && mx < this.xPosition + this.width && my < this.yPosition + this.height;
            int k = this.getHoverState(this.field_146123_n);
            
            if(k == 1) {
            	mc.getTextureManager().bindTexture(this.button);
            	float f = 0.00390625F / 3.13F;
                float f1 = 0.00390625F / 0.4F;
                Tessellator tessellator = Tessellator.instance;
                tessellator.startDrawingQuads();
                tessellator.addVertexWithUV(this.xPosition		       , this.yPosition				 , this.zLevel, 0  	   , 0);
                tessellator.addVertexWithUV(this.xPosition			   , this.yPosition + this.height, this.zLevel, 0  	   , 100 * f1);
                tessellator.addVertexWithUV(this.xPosition + this.width, this.yPosition + this.height, this.zLevel, 800 * f, 100 * f1);
                tessellator.addVertexWithUV(this.xPosition + this.width, this.yPosition				 , this.zLevel, 800 * f, 0);
                tessellator.draw();
            } else if(k == 2) {
            	mc.getTextureManager().bindTexture(this.button_hover);
            	float f = 0.00390625F / 3.13F;
                float f1 = 0.00390625F / 0.4F;
                Tessellator tessellator = Tessellator.instance;
                tessellator.startDrawingQuads();
                tessellator.addVertexWithUV(this.xPosition		       , this.yPosition				 , this.zLevel, 0  	   , 0);
                tessellator.addVertexWithUV(this.xPosition			   , this.yPosition + this.height, this.zLevel, 0  	   , 100 * f1);
                tessellator.addVertexWithUV(this.xPosition + this.width, this.yPosition + this.height, this.zLevel, 800 * f, 100 * f1);
                tessellator.addVertexWithUV(this.xPosition + this.width, this.yPosition				 , this.zLevel, 800 * f, 0);
                tessellator.draw();
            }
            
            float x = this.xPosition * 6.71F + (this.width * 6.53F) / 2 - ClientProxy.customFont1.getStringWidth(this.displayString) / 2;
            ClientProxy.customFont1.drawString(x, (this.yPosition + (this.height - 8) / 2) * 6.7F, this.displayString, k == 1 ? Color.black : Color.white, 0.15F);
        }
    }
	
}
