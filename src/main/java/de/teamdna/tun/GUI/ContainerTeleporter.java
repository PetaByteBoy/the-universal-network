package de.teamdna.tun.GUI;

import java.util.List;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerEnchantment;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.TileEntity.TileEntityTeleporter;
import de.teamdna.tun.Util.WorldBlock;

public class ContainerTeleporter extends Container {

	public WorldBlock block;
	public EntityPlayer player;

	private List<TileEntity> teleporter;
	
	public ContainerTeleporter(World world, int x, int y, int z, EntityPlayer player) {
		this.block = new WorldBlock(world, x, y, z);
		this.player = player;
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return true;
	}

}
