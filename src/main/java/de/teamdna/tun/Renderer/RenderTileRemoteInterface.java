package de.teamdna.tun.Renderer;

import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.tileentity.TileEntity;
import de.teamdna.tun.TileEntity.TileEntityRemoteInterface;
import de.teamdna.tun.Util.WorldBlock;

public class RenderTileRemoteInterface extends RenderTileCore {

	@Override
	public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float var8) {
		TileEntityRemoteInterface remote = (TileEntityRemoteInterface)tile;
		if(remote.hasWorldObj() && !remote.getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(remote.getLinkedBlock());
			
			if(block.hasTile() && TileEntityRendererDispatcher.instance.hasSpecialRenderer(block.tile)) {
				TileEntityRendererDispatcher.instance.renderTileEntityAt(block.tile, x, y, z, var8);
			}
		}
	}

}
