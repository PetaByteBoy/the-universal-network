package de.teamdna.tun.Renderer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityBeaconRenderer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityBeacon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

import de.teamdna.tun.TUN;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.Util.Reference;

public class RenderTileCore extends TileEntitySpecialRenderer {

	private Minecraft mc;
	private ResourceLocation white = new ResourceLocation(Reference.modid, "textures/blocks/white.png");
	
	public RenderTileCore() {
		this.mc = Minecraft.getMinecraft();
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float var8) {
//		Color color;
//		
//		INetworkComponent component = (INetworkComponent)tile;
//		if(component.isLinked()) color = Color.green;
//		else color = Color.transparent;
//		
//		if(color != Color.transparent) {
//			ItemStack goggles = this.mc.thePlayer.inventory.armorItemInSlot(3);
//			if(goggles != null && Item.itemRegistry.getIDForObject(goggles.getItem()) == Item.itemRegistry.getIDForObject(TUN.goggles)) {
//				GL11.glPushMatrix();
//				GL11.glEnable(GL11.GL_BLEND);
//		        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
//		        GL11.glDisable(GL11.GL_TEXTURE_2D);
//		        GL11.glDisable(GL11.GL_LIGHTING);
//		        GL11.glDisable(GL11.GL_CULL_FACE);
//		        GL11.glDisable(GL11.GL_DEPTH_TEST);
//		        GL11.glDepthMask(false);
//
//	            double offset = -0.1D;
//				double d = 1 + offset * 2;
//				
//				GL11.glTranslated(x - offset, y - offset, z - offset);
//				GL11.glColor4f(color.r, color.g, color.b, 0.8F);
//				GL11.glBegin(GL11.GL_QUADS);
//				
//				GL11.glVertex3d(0, 0, 0);
//				GL11.glVertex3d(0, 0, d);
//				GL11.glVertex3d(0, d, d);
//				GL11.glVertex3d(0, d, 0);
//				
//				GL11.glVertex3d(0, 0, 0);
//				GL11.glVertex3d(0, d, 0);
//				GL11.glVertex3d(d, d, 0);
//				GL11.glVertex3d(d, 0, 0);
//				
//				GL11.glVertex3d(d, 0, 0);
//				GL11.glVertex3d(d, d, 0);
//				GL11.glVertex3d(d, d, d);
//				GL11.glVertex3d(d, 0, d);
//				
//				GL11.glVertex3d(0, 0, d);
//				GL11.glVertex3d(d, 0, d);
//				GL11.glVertex3d(d, d, d);
//				GL11.glVertex3d(0, d, d);
//				
//				GL11.glVertex3d(0, d, 0);
//				GL11.glVertex3d(0, d, d);
//				GL11.glVertex3d(d, d, d);
//				GL11.glVertex3d(d, d, 0);
//				
//				GL11.glVertex3d(0, 0, 0);
//				GL11.glVertex3d(d, 0, 0);
//				GL11.glVertex3d(d, 0, d);
//				GL11.glVertex3d(0, 0, d);
//				GL11.glEnd();
//	
//				GL11.glDepthMask(true);
//		        GL11.glEnable(GL11.GL_DEPTH_TEST);
//		        GL11.glEnable(GL11.GL_CULL_FACE);
//		        GL11.glEnable(GL11.GL_LIGHTING);
//		        GL11.glEnable(GL11.GL_TEXTURE_2D);
//		        GL11.glDisable(GL11.GL_BLEND);
//				GL11.glPopMatrix();
//			}
//		}
	}
	
}
