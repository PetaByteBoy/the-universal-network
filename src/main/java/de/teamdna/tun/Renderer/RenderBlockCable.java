package de.teamdna.tun.Renderer;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.world.IBlockAccess;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import de.teamdna.tun.Blocks.BlockCable;
import de.teamdna.tun.Net.ClientProxy;
import de.teamdna.tun.Network.INetworkComponent;

public class RenderBlockCable implements ISimpleBlockRenderingHandler {

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer) {
		GL11.glRotatef(90F, 0, 1, 0);
		
		block.setBlockBounds(0F, 0.3F, 0.3F, 1F, 0.7F, 0.7F);
		renderer.setRenderBoundsFromBlock(block);
		
		Tessellator tessellator = Tessellator.instance;
		GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, -1F, 0.0F);
		renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, 0));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, 0.0F);
		renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, 0));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 0.0F, -1F);
		renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, 0));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 0.0F, 1.0F);
		renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, 0));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(-1F, 0.0F, 0.0F);
		renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, 0));
		tessellator.draw();
		tessellator.startDrawingQuads();
		tessellator.setNormal(1.0F, 0.0F, 0.0F);
		renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, 0));
		tessellator.draw();
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
		boolean flag1 = false;
        boolean flag2 = false;
        boolean flag0 = false;

        if(BlockCable.isConnectable(world, x - 1, y, z) || BlockCable.isConnectable(world, x + 1, y, z)) flag1 = true;
        if(BlockCable.isConnectable(world, x, y, z - 1) || BlockCable.isConnectable(world, x, y, z + 1)) flag2 = true;
        if(BlockCable.isConnectable(world, x, y - 1, z) || BlockCable.isConnectable(world, x, y + 1, z)) flag0 = true;

        boolean flag3 = BlockCable.isConnectable(world, x - 1, y, z);
        boolean flag4 = BlockCable.isConnectable(world, x + 1, y, z);
        boolean flag5 = BlockCable.isConnectable(world, x, y, z - 1);
        boolean flag6 = BlockCable.isConnectable(world, x, y, z + 1);
        boolean flag7 = BlockCable.isConnectable(world, x, y - 1, z);
        boolean flag8 = BlockCable.isConnectable(world, x, y + 1, z);

        if(!flag1 && !flag2) flag1 = true;

        float f4 = flag3 ? 0.0F : 0.35F;
        float f5 = flag4 ? 1.0F : 0.6F;
        float f6 = flag5 ? 0.0F : 0.35F;
        float f7 = flag6 ? 1.0F : 0.6F;
        float f8 = flag7 ? 0.0F : 0.35F;
        float f9 = flag8 ? 1.0F : 0.6F;
        
        if(flag1) {
        	renderer.setRenderBounds(f4, 0.35F, 0.35F, f5, 0.6F, 0.6F);
            renderer.renderStandardBlock(block, x, y, z);
        }
        if(flag2) {
        	renderer.setRenderBounds(0.35F, 0.35F, f6, 0.6F, 0.6F, f7);
            renderer.renderStandardBlock(block, x, y, z);
        }
        if(flag0) {
        	renderer.setRenderBounds(0.35F, f8, 0.35F, 0.6F, f9, 0.6F);
            renderer.renderStandardBlock(block, x, y, z);
        }
        
        return true;
	}
	
	@Override
	public boolean shouldRender3DInInventory(int modelId) {
		return true;
	}

	@Override
	public int getRenderId() {
		return ClientProxy.renderBlockIOCableID;
	}

}
