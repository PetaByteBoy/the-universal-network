package de.teamdna.tun.TileEntity;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.Ticket;
import net.minecraftforge.common.ForgeChunkManager.Type;
import net.minecraftforge.common.util.ForgeDirection;
import de.teamdna.tun.TUN;
import de.teamdna.tun.Event.WorldEventHandler;
import de.teamdna.tun.Net.Packet.PacketLinkSync;
import de.teamdna.tun.Network.IChunkLoader;
import de.teamdna.tun.Network.IMachine;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.Util.Util;
import de.teamdna.tun.Util.WorldBlock;

public class TileEntityNetworkManager extends TileEntity implements IChunkLoader {

	public Ticket ticket;
	
	public List<WorldBlock> queuedNetworkChecks = new ArrayList<WorldBlock>();
	public List<String> checkedBlocks = new ArrayList<String>();
	public List<WorldBlock> notifiedController = new ArrayList<WorldBlock>();
	
	public List<String> allLinkedTiles = new ArrayList<String>();
	public List<String> linkedCables = new ArrayList<String>();
	public List<String> linkedMachines = new ArrayList<String>();
	public List<String> linkedAntennas = new ArrayList<String>();
	
	public boolean interruptWork = false;
	private boolean isFirstTick = true;
	private boolean forceCreate = false;
	private boolean searchedForSubNetworks = false;
	
	private int teleporterCount = 0;
	
	public void registerComponent(String uid) {
		this.allLinkedTiles.add(uid);
		this.recreateNetwork();
	}
	
	public void unregisterComponent(String uid) {
		this.allLinkedTiles.remove(uid);
		this.recreateNetwork();
	}
	
	public String getLink() {
		return Util.createBlockUID(this.worldObj, this.xCoord, this.yCoord, this.zCoord);
	}
	
	public void recreateNetwork() {
		this.destroyNetwork();
		this.createNetwork();
	}
	
	public void updateNetwork() {
		if(this.queuedNetworkChecks.get(0) != null) {
			TileEntity tile = this.queuedNetworkChecks.get(0).tile;
			if(tile != null && tile instanceof INetworkComponent) {
				((INetworkComponent)tile).setLink(this.getLink());
				this.allLinkedTiles.add(Util.createBlockUID(tile.getWorldObj(), tile.xCoord, tile.yCoord, tile.zCoord));
				
				if(tile instanceof TileEntityCable) {
					this.linkedCables.add(Util.createBlockUID(tile.getWorldObj(), tile.xCoord, tile.yCoord, tile.zCoord));
				}
				
				if(tile instanceof IMachine) {
					this.linkedMachines.add(Util.createBlockUID(tile.getWorldObj(), tile.xCoord, tile.yCoord, tile.zCoord));
					
					NBTTagCompound syncTag = new NBTTagCompound();
					tile.writeToNBT(syncTag);
					TUN.packetPipeline.sendToAll(new PacketLinkSync(tile.getWorldObj(), tile.xCoord, tile.yCoord, tile.zCoord, this.getLink(), syncTag));
					
					if(tile instanceof TileEntityTeleporter) {
						TileEntityTeleporter teleporter = (TileEntityTeleporter)tile;
						if(!teleporter.customName) {
							this.teleporterCount++;
							teleporter.name = "Teleporter" + String.valueOf(this.teleporterCount);
						}
					} else if(tile instanceof TileEntityAntenna) {
						this.linkedAntennas.add(Util.createBlockUID(tile.getWorldObj(), tile.xCoord, tile.yCoord, tile.zCoord));
					}
				}
			}
			this.queuedNetworkChecks.remove(0);
			this.findAndAddNeighborsToQueue(tile.getWorldObj(), tile.xCoord, tile.yCoord, tile.zCoord);
		}
	}
	
	public void destroyNetwork() {
		for(String s : this.allLinkedTiles) {
			Object[] uid = Util.getBlockFromUID(s);
			World world = DimensionManager.getWorld(Integer.parseInt(String.valueOf(uid[0])));
			int x = Integer.parseInt(String.valueOf(uid[1]));
			int y = Integer.parseInt(String.valueOf(uid[2]));
			int z = Integer.parseInt(String.valueOf(uid[3]));
			
			TileEntity tile = world.getTileEntity(x, y, z);
			if(tile != null && tile instanceof INetworkComponent) {
				((INetworkComponent)tile).setLink("");
			}
		}
		
		this.allLinkedTiles.clear();
		this.linkedCables.clear();
		this.linkedMachines.clear();
		this.linkedAntennas.clear();
		
		this.teleporterCount = 0;
		this.searchedForSubNetworks = false;
	}
	
	public void findAndAddNeighborsToQueue(World world, int x, int y, int z) {
		for(ForgeDirection dir : ForgeDirection.VALID_DIRECTIONS) {
			TileEntity tile = world.getTileEntity(x + dir.offsetX, y + dir.offsetY, z + dir.offsetZ);
			String uid = Util.createBlockUID(world, x + dir.offsetX, y + dir.offsetY, z + dir.offsetZ);
			if(tile != null && tile instanceof INetworkComponent && !this.checkedBlocks.contains(uid)) {
				this.queuedNetworkChecks.add(new WorldBlock(world, x + dir.offsetX, y + dir.offsetY, z + dir.offsetZ));
				this.checkedBlocks.add(uid);
			}
		}
	}
	
	public void createNetwork() {
		this.allLinkedTiles.clear();
		this.linkedCables.clear();
		this.linkedMachines.clear();
		this.linkedAntennas.clear();
		
		this.queuedNetworkChecks.clear();
		this.checkedBlocks.clear();
		this.findAndAddNeighborsToQueue(this.worldObj, this.xCoord, this.yCoord, this.zCoord);
	}
	
	public void findSubNetworks() {
		this.searchedForSubNetworks = true;
		for(String s : this.linkedAntennas) {
			WorldBlock block = new WorldBlock(s);
			TileEntityAntenna tile = (TileEntityAntenna)block.tile;
			
			if(!tile.linkedAntenna.equals("")) {
				WorldBlock link = new WorldBlock(tile.linkedAntenna);
				this.updateSubNetwork(link.world, link.x, link.y, link.z);
			}
		}
	}
	
	public void notifyControllerDetection(World world, int x, int y, int z) {
		this.notifiedController.add(new WorldBlock(world, x, y, z));
		this.interruptWork = true;
	}
	
	public List<TileEntity> getNetworkTilesOfType(Class type) {
		List<TileEntity> output = new ArrayList<TileEntity>();
		for(String s : this.linkedMachines) {
			WorldBlock block = new WorldBlock(s);
			if(block.tile != null && block.tile.getClass().equals(type)) output.add(block.tile);
		}
		
		return output;
	}
	
	public boolean canWork() {
		return !this.interruptWork;
	}
	
	public void updateSubNetwork(World world, int x, int y, int z) {
		this.findAndAddNeighborsToQueue(world, x, y, z);
	}
	
	@Override
	public void updateEntity() {
		if(this.worldObj.isRemote) return;

		if(!this.interruptWork) {
			if(this.isFirstTick || this.forceCreate) {
				this.isFirstTick = false;
				this.forceCreate = false;
				
				WorldEventHandler.startedBuilding(this);
				
				for(ForgeDirection dir : ForgeDirection.VALID_DIRECTIONS) {
					TileEntity tile = this.worldObj.getTileEntity(this.xCoord + dir.offsetX, this.yCoord + dir.offsetY, this.zCoord + dir.offsetZ);
					if(tile != null && tile instanceof TileEntityNetworkManager) {
						this.notifyControllerDetection(this.worldObj, this.xCoord + dir.offsetX, this.yCoord + dir.offsetY, this.zCoord + dir.offsetZ);
						return;
					}
				}
				
				this.createNetwork();
			}
			
			if(this.queuedNetworkChecks.size() > 0) this.updateNetwork();
			else if(!this.searchedForSubNetworks) this.findSubNetworks();
			else WorldEventHandler.finishedBuilding(this);
		} else {
			if(this.notifiedController.size() > 0) {
				WorldBlock block = this.notifiedController.get(0);
				TileEntity tile = block.updateTileEntity();
				if(tile == null || (tile != null && tile instanceof TileEntityNetworkManager)) {
					this.notifiedController.remove(0);
				}
			} else {
				this.interruptWork = false;
			}
		}
	}
	
	@Override
	public void validate() {
		super.validate();
		this.forceCreate = true;
		
		if(this.ticket == null) this.ticket = ForgeChunkManager.requestTicket(TUN.INSTANCE, this.worldObj, Type.NORMAL);
		
		this.ticket.getModData().setInteger("bx", this.xCoord);
		this.ticket.getModData().setInteger("by", this.yCoord);
		this.ticket.getModData().setInteger("bz", this.zCoord);
		ForgeChunkManager.forceChunk(this.ticket, new ChunkCoordIntPair(this.xCoord >> 4, this.zCoord >> 4));
		this.loadChunks(this.ticket);
	}
	
	@Override
	public void invalidate() {
		this.destroyNetwork();
		ForgeChunkManager.releaseTicket(this.ticket);
		super.invalidate();
	}
	
	@Override
	public void loadChunks(Ticket ticket1) {
		if(this.ticket == null) this.ticket = ticket1;
		ChunkCoordIntPair chunk = new ChunkCoordIntPair(this.xCoord >> 4, this.zCoord >> 4);
		ForgeChunkManager.forceChunk(ticket1, chunk);
	}
	
}
