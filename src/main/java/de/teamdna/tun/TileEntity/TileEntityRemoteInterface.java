package de.teamdna.tun.TileEntity;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import de.teamdna.tun.TUN;
import de.teamdna.tun.Blocks.BlockRemoteInterface;
import de.teamdna.tun.Network.IMachine;
import de.teamdna.tun.Util.AABBHelper;
import de.teamdna.tun.Util.WorldBlock;

public class TileEntityRemoteInterface extends TileEntityCore implements IMachine, ISidedInventory {

	private String linkedBlock = "";
	
	public String getLinkedBlock() {
		return this.linkedBlock;
	}
	
	public void linkToBlock(String uid) {
		this.linkedBlock = uid;
		this.worldObj.setBlockMetadataWithNotify(this.xCoord, this.yCoord, this.zCoord, (new WorldBlock(uid)).meta, 2);
		Minecraft.getMinecraft().renderGlobal.markBlockForRenderUpdate(this.xCoord, this.yCoord, this.zCoord);
	}
	
	public void unlink() {
		this.linkedBlock = "";
		this.worldObj.setBlockMetadataWithNotify(this.xCoord, this.yCoord, this.zCoord, 0, 2);
		BlockRemoteInterface.updateBlockState(this.worldObj, this.xCoord, this.yCoord, this.zCoord);
		Minecraft.getMinecraft().renderGlobal.markBlockForRenderUpdate(this.xCoord, this.yCoord, this.zCoord);
	}
	
	public TileEntity getLinkedTile() {
		if(!this.getLinkedBlock().equals("")) {
    		WorldBlock block = new WorldBlock(this.getLinkedBlock());
    		if(block.hasTile()) return block.tile;
    	}
		
		return null;
	}
	
	public ISidedInventory getLinkedInventory() {
		if(this.getLinkedTile() != null && this.getLinkedTile() instanceof ISidedInventory) return (ISidedInventory)this.getLinkedTile();
		return null;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound tag) {
		super.readFromNBT(tag);
		this.linkedBlock = tag.getString("link");
    }

	@Override
    public void writeToNBT(NBTTagCompound tag) {
		super.writeToNBT(tag);
    	tag.setString("link", this.linkedBlock);
    }
    
    @Override
    public Packet getDescriptionPacket() {
    	NBTTagCompound nbttagcompound = new NBTTagCompound();
        this.writeToNBT(nbttagcompound);
        return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 3, nbttagcompound);
    }
    
    @Override
    public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
    	this.readFromNBT(pkt.func_148857_g());
    }
    
    @Override
    public void updateEntity() {
    	super.updateEntity();
    	if(this.worldObj.isRemote || this.getLinkedBlock().equals("")) return;
    	
    	WorldBlock block = new WorldBlock(this.getLinkedBlock());
    	if(!block.exists() || !block.hasTile()) {
    		this.unlink();
    	}
    	
    	if(block.meta != this.getBlockMetadata()) {
    		this.worldObj.setBlockMetadataWithNotify(this.xCoord, this.yCoord, this.zCoord, block.meta, 2);
    	}
    }
    
    /* ======================== COMPATIBILITY FUNCTIONS STARTS ======================== */
    
    @Override
    @SideOnly(Side.CLIENT)
    public AxisAlignedBB getRenderBoundingBox() {
    	TileEntity tile = this.getLinkedTile();
    	if(tile != null) return AABBHelper.translateEntityAABB(tile.getRenderBoundingBox(), tile.xCoord, tile.yCoord, tile.zCoord, this.xCoord, this.yCoord, this.zCoord);
    	return super.getRenderBoundingBox();
    }
    
    
    /* == ISidedInventory - Start == */
    
	@Override
	public int getSizeInventory() {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().getSizeInventory();
		return 0;
	}

	@Override
	public ItemStack getStackInSlot(int var1) {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().getStackInSlot(var1);
		return null;
	}

	@Override
	public ItemStack decrStackSize(int var1, int var2) {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().decrStackSize(var1, var2);
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1) {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().getStackInSlotOnClosing(var1);
		return null;
	}

	@Override
	public void setInventorySlotContents(int var1, ItemStack var2) {
		if(this.getLinkedInventory() != null) this.getLinkedInventory().setInventorySlotContents(var1, var2);
	}

	@Override
	public String getInventoryName() {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().getInventoryName();
		return "remoteInterface";
	}

	@Override
	public boolean hasCustomInventoryName() {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().hasCustomInventoryName();
		return false;
	}

	@Override
	public int getInventoryStackLimit() {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().getInventoryStackLimit();
		return 0;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer var1) {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().isUseableByPlayer(var1);
		return false;
	}

	@Override
	public void openInventory() {
		if(this.getLinkedInventory() != null) this.getLinkedInventory().openInventory();
	}

	@Override
	public void closeInventory() {
		if(this.getLinkedInventory() != null) this.getLinkedInventory().closeInventory();
	}

	@Override
	public boolean isItemValidForSlot(int var1, ItemStack var2) {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().isItemValidForSlot(var1, var2);
		return false;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().getAccessibleSlotsFromSide(var1);
		return new int[] {};
	}

	@Override
	public boolean canInsertItem(int var1, ItemStack var2, int var3) {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().canInsertItem(var1, var2, var3);
		return false;
	}

	@Override
	public boolean canExtractItem(int var1, ItemStack var2, int var3) {
		if(this.getLinkedInventory() != null) return this.getLinkedInventory().canExtractItem(var1, var2, var3);
		return false;
	}
	
	/* == ISidedInventory - End == */

	/* ======================== COMPATIBILITY FUNCTIONS ENDS ======================== */
	
}
