package de.teamdna.tun.TileEntity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import de.teamdna.tun.Network.IMachine;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.Util.WorldBlock;

public class TileEntityAntenna extends TileEntityCore implements IMachine {

	public String linkedAntenna = "";
	
	@Override
	public void readFromNBT(NBTTagCompound tag) {
		super.readFromNBT(tag);
		this.linkedAntenna = tag.getString("linkedAntenna");
    }
	
	@Override
    public void writeToNBT(NBTTagCompound tag) {
		super.writeToNBT(tag);
		tag.setString("linkedAntenna", this.linkedAntenna);
    }
	
	public void linkToAntenna(String uid) {
		this.linkedAntenna = uid;
	}

	public void handleAntennaBreaking() {
		if(!this.linkedAntenna.equals("")) {
			WorldBlock block = new WorldBlock(this.linkedAntenna);
			if(block.hasTile()) ((TileEntityAntenna)block.tile).linkedAntenna = "";
		}
	}
	
}
