package de.teamdna.tun.TileEntity;

import net.minecraft.tileentity.TileEntity;
import de.teamdna.tun.Event.WorldEventHandler;
import de.teamdna.tun.Network.INetworkComponent;

public class TileEntityCore extends TileEntity implements INetworkComponent {

	public String link = "";
	public int tickCount = 0;
	
	@Override
	public boolean isLinked() {
		return !this.link.equals("");
	}
	
	@Override
	public void setLink(String link) {
		this.link = link;
	}
	
	@Override
	public String getLink() {
		return this.link;
	}
	
	@Override
	public boolean canUpdate() {
		return true;
	}
	
	@Override
	public void updateEntity() {
		if(this.worldObj.isRemote || this.isLinked() || this.tickCount > 4) return;
		if(++this.tickCount == 4) WorldEventHandler.requestManagerRebuilds();
	}
	
	@Override
	public void validate() {
		super.validate();
	}
	
	@Override
	public void invalidate() {
		super.invalidate();
	}
	
}
