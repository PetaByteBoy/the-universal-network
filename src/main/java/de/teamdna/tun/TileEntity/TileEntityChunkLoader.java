package de.teamdna.tun.TileEntity;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.AnvilChunkLoader;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.Ticket;
import net.minecraftforge.common.ForgeChunkManager.Type;
import de.teamdna.tun.TUN;
import de.teamdna.tun.Network.IChunkLoader;

public class TileEntityChunkLoader extends TileEntity implements IChunkLoader {

	public Ticket ticket;
	
	@Override
	public void validate() {
		super.validate();
		if(this.ticket == null) this.ticket = ForgeChunkManager.requestTicket(TUN.INSTANCE, this.worldObj, Type.NORMAL);
		
		this.ticket.getModData().setInteger("bx", this.xCoord);
		this.ticket.getModData().setInteger("by", this.yCoord);
		this.ticket.getModData().setInteger("bz", this.zCoord);
		ForgeChunkManager.forceChunk(this.ticket, new ChunkCoordIntPair(this.xCoord >> 4, this.zCoord >> 4));
		this.loadChunks(this.ticket);
	}
	
	@Override
	public void invalidate() {
		super.invalidate();
		ForgeChunkManager.releaseTicket(this.ticket);
	}
	
	@Override
	public boolean canUpdate() {
		return false;
	}

	@Override
	public void loadChunks(Ticket ticket1) {
		if(this.ticket == null) this.ticket = ticket1;
		ChunkCoordIntPair chunk = new ChunkCoordIntPair(this.xCoord >> 4, this.zCoord >> 4);
		ForgeChunkManager.forceChunk(ticket1, chunk);
	}
	
}
