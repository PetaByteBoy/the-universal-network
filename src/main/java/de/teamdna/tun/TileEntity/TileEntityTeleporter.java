package de.teamdna.tun.TileEntity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import de.teamdna.tun.TUN;
import de.teamdna.tun.Net.Packet.PacketTeleportRequest;
import de.teamdna.tun.Network.IMachine;
import de.teamdna.tun.Network.INetworkComponent;

public class TileEntityTeleporter extends TileEntityCore implements IMachine {
	
	public String name = "";
	public boolean customName = false;
	
	@Override
	public void readFromNBT(NBTTagCompound tag) {
		super.readFromNBT(tag);
		this.name = tag.getString("name");
		this.customName = tag.getBoolean("custom");
    }
	
	@Override
    public void writeToNBT(NBTTagCompound tag) {
		super.writeToNBT(tag);
		tag.setString("name", this.name);
		tag.setBoolean("custom", this.customName);
    }
	
}
