package de.teamdna.tun.Blocks;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import de.teamdna.tun.Net.ClientProxy;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.TileEntity.TileEntityCable;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.Util.Reference;

public class BlockCable extends BlockCore {

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityCable();
	}

	@Override
	@SideOnly(Side.CLIENT)
    public int getRenderType() {
        return ClientProxy.renderBlockIOCableID;
    }

	@Override
    public boolean renderAsNormalBlock() {
        return false;
    }

	@Override
    public boolean isOpaqueCube() {
        return false;
    }
    
	@Override
    public int getMobilityFlag() {
        return 2;
    }
	
	@Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
    	float width = 0.6F;
		float height = 0.6F;
		float deepness = 0.6F;
		float minX = 0.35F;
		float minY = 0.35F;
		float minZ = 0.35F;
        
        if(this.isConnectable(world, x - 1, y, z)) minX = 0F;
        if(this.isConnectable(world, x + 1, y, z)) width = 1F;
        if(this.isConnectable(world, x, y, z - 1)) minZ = 0F;
        if(this.isConnectable(world, x, y, z + 1)) deepness = 1F;
        if(this.isConnectable(world, x, y - 1, z)) minY = 0F;
        if(this.isConnectable(world, x, y + 1, z)) height = 1F;
        
        this.setBlockBounds(minX, minY, minZ, width, height, deepness);
    }
    
    public static boolean isConnectable(IBlockAccess world, int x, int y, int z) {
        return world.getTileEntity(x, y, z) instanceof INetworkComponent ||
        		world.getTileEntity(x, y, z) instanceof TileEntityNetworkManager;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess world, int x, int y, int z, int meta) {
        return true;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register) {
    	this.blockIcon = register.registerIcon(Reference.modid + ":cable");
    }
}
