package de.teamdna.tun.Blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import de.teamdna.tun.Event.WorldEventHandler;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.TileEntity.TileEntityCable;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.Util.Util;

public class BlockNetworkManager extends BlockContainer {
	
	public BlockNetworkManager() {
		super(Material.iron);
		this.setStepSound(Block.soundTypePiston);
	}
	
	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityNetworkManager();
	}
	
	@Override
    public int getMobilityFlag() {
        return 2;
    }

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityNetworkManager) {
			((TileEntityNetworkManager)tile).recreateNetwork();
		}
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		WorldEventHandler.removeChunkLoader(world.getTileEntity(x, y, z));
		
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityNetworkManager) {
			((TileEntityNetworkManager)tile).destroyNetwork();
		}
		
        super.breakBlock(world, x, y, z, block, meta);
    }
	
	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
        super.onBlockAdded(world, x, y, z);
		WorldEventHandler.addChunkLoader(world.getTileEntity(x, y, z));
    }
	
}
