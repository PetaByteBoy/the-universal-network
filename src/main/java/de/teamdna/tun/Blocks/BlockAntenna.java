package de.teamdna.tun.Blocks;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import de.teamdna.tun.TUN;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.TileEntity.TileEntityAntenna;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.Util.Util;
import de.teamdna.tun.Util.WorldBlock;

public class BlockAntenna extends BlockCore {

	public BlockAntenna() {
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityAntenna();
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityAntenna) {
			((TileEntityAntenna)tile).handleAntennaBreaking();
		}
		
        super.breakBlock(world, x, y, z, block, meta);
    }
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int meta, float hitX, float hitY, float hitZ) {
		if(!world.isRemote) {
			TileEntityAntenna tile = (TileEntityAntenna)world.getTileEntity(x, y, z);
			
			if(player.inventory.getCurrentItem() != null && Item.itemRegistry.getIDForObject(player.inventory.getCurrentItem().getItem()) == Item.itemRegistry.getIDForObject(TUN.linkCard)) {
				if(player.inventory.getCurrentItem().getTagCompound() == null && tile.isLinked()) {
					NBTTagCompound tag = new NBTTagCompound();
					tag.setString("link", tile.getLink());
					tag.setString("link_antenna", Util.createBlockUID(world, x, y, z));
					player.inventory.getCurrentItem().setTagCompound(tag);
					player.inventory.getCurrentItem().setItemDamage(1);
					
					return true;
				} else {
					if(player.inventory.getCurrentItem().getTagCompound() != null && player.inventory.getCurrentItem().getTagCompound().hasKey("link")) {
						if(!player.inventory.getCurrentItem().getTagCompound().getString("link_antenna").equals(Util.createBlockUID(world, x, y, z))) {
							tile.setLink(player.inventory.getCurrentItem().getTagCompound().getString("link"));
							tile.linkToAntenna(player.inventory.getCurrentItem().getTagCompound().getString("link_antenna"));
							
							WorldBlock antenna = new WorldBlock(player.inventory.getCurrentItem().getTagCompound().getString("link_antenna"));
							if(antenna.hasTile() && antenna.tile instanceof TileEntityAntenna) {
								((TileEntityAntenna)antenna.tile).setLink(tile.getLink());
								((TileEntityAntenna)antenna.tile).linkToAntenna(Util.createBlockUID(world, x, y, z));
							}
							
							WorldBlock manager = new WorldBlock(tile.getLink());
		        			if(manager.hasTile() && manager.tile instanceof TileEntityNetworkManager) {
		        				((TileEntityNetworkManager)manager.tile).recreateNetwork();
		        			}
						}
	        			player.inventory.getCurrentItem().setTagCompound(null);
	        			player.inventory.getCurrentItem().setItemDamage(0);
	        			
	        			return true;
					}
				}
			}
		}
		
        return super.onBlockActivated(world, x, y, z, player, meta, hitX, hitY, hitZ);
    }
	
}
