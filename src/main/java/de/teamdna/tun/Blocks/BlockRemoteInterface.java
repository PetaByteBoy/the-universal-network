package de.teamdna.tun.Blocks;

import java.util.List;
import java.util.Random;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import de.teamdna.tun.TUN;
import de.teamdna.tun.Net.ClientProxy;
import de.teamdna.tun.TileEntity.TileEntityRemoteInterface;
import de.teamdna.tun.Util.Reference;
import de.teamdna.tun.Util.WorldBlock;

public class BlockRemoteInterface extends BlockCore {
	
	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityRemoteInterface();
	}

	@Override
	@SideOnly(Side.CLIENT)
    public int getRenderType() {
        return ClientProxy.renderBlockRemoteInterfaceID;
    }

	@Override
    public boolean renderAsNormalBlock() {
        return false;
    }

	@Override
    public boolean isOpaqueCube() {
        return false;
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister register) {
		this.blockIcon = register.registerIcon(Reference.modid + ":white");
	}
	
	@Override
    public int getMobilityFlag() {
        return 2;
    }
	
	public static void updateBlockState(World world, int x, int y, int z) {
        int meta = world.getBlockMetadata(x, y, z);
        TileEntity tile = world.getTileEntity(x, y, z);

        world.setBlock(x, y, z, TUN.remoteInterface);
        world.setBlockMetadataWithNotify(x, y, z, meta, 2);

        if (tile != null) {
        	tile.validate();
        	world.setTileEntity(x, y, z, tile);
        }
        
        world.markBlockForUpdate(x, y, z);
    }
	
	@SideOnly(Side.CLIENT)
    public void randomDisplayTick(World world, int x, int y, int z, Random rnd) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			block.getBlock().randomDisplayTick(world, x, y, z, rnd);
			return;
		}
		
		super.randomDisplayTick(world, x, y, z, rnd);
	}
	
	/* ======================== COMPATIBILITY FUNCTIONS STARTS ======================== */

	@Override
    public boolean getBlocksMovement(IBlockAccess world, int x, int y, int z) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			return block.getBlock().getBlocksMovement(world, x, y, z);
		}
		
		return super.getBlocksMovement(world, x, y, z);
    }
    
	@Override
    public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB aabb, List list, Entity entity) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			block.getBlock().addCollisionBoxesToList(world, x, y, z, aabb, list, entity);
			return;
		}
		
		super.addCollisionBoxesToList(world, x, y, z, aabb, list, entity);
    }

	@Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			return block.getBlock().getCollisionBoundingBoxFromPool(world, x, y, z);
		}
		
		return super.getCollisionBoundingBoxFromPool(world, x, y, z);
    }
    
	@Override
    @SideOnly(Side.CLIENT)
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			return block.getBlock().getSelectedBoundingBoxFromPool(world, x, y, z);
		}
		
		return super.getSelectedBoundingBoxFromPool(world, x, y, z);
    }
    
	@Override
    @SideOnly(Side.CLIENT)
    public int colorMultiplier(IBlockAccess world, int x, int y, int z) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			return block.getBlock().colorMultiplier(world, x, y, z);
		}
		
		return super.colorMultiplier(world, x, y, z);
    }

	@Override
    public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int p_149709_5_) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			return block.getBlock().isProvidingWeakPower(world, x, y, z, p_149709_5_);
		}
		
		return super.isProvidingWeakPower(world, x, y, z, p_149709_5_);
    }

	@Override
    public boolean canProvidePower() {
        return true;
    }
    
	@Override
    public int isProvidingStrongPower(IBlockAccess world, int x, int y, int z, int p_149748_5_) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			return block.getBlock().isProvidingStrongPower(world, x, y, z, p_149748_5_);
		}
		
		return super.isProvidingStrongPower(world, x, y, z, p_149748_5_);
    }
    
	@Override
    public boolean onBlockEventReceived(World world, int x, int y, int z, int p_149696_5_, int p_149696_6_) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			return block.getBlock().onBlockEventReceived(world, x, y, z, p_149696_5_, p_149696_6_);
		}
		
		return super.onBlockEventReceived(world, x, y, z, p_149696_5_, p_149696_6_);
    }
    
	@Override
    public boolean hasComparatorInputOverride() {
        return true;
    }

	@Override
    public int getComparatorInputOverride(World world, int x, int y, int z, int p_149736_5_) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileEntityRemoteInterface && !((TileEntityRemoteInterface)tile).getLinkedBlock().equals("")) {
			WorldBlock block = new WorldBlock(((TileEntityRemoteInterface)tile).getLinkedBlock());
			return block.getBlock().getComparatorInputOverride(world, x, y, z, p_149736_5_);
		}
		
		return super.getComparatorInputOverride(world, x, y, z, p_149736_5_);
    }
	
	/* ======================== COMPATIBILITY FUNCTIONS ENDS ======================== */
	
}
