package de.teamdna.tun.Blocks;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import de.teamdna.tun.TUN;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.TileEntity.TileEntityTeleporter;
import de.teamdna.tun.Util.Reference;

public class BlockTeleporter extends BlockCore {

	@SideOnly(Side.CLIENT)
	public IIcon blockTop, blockBottom;
	
	public BlockTeleporter() {
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityTeleporter();
	}

	@Override
	public void onEntityWalking(World world, int x, int y, int z, Entity entity) {
		super.onEntityWalking(world, x, y, z, entity);
		
		if(((INetworkComponent)world.getTileEntity(x, y, z)).isLinked() && entity instanceof EntityPlayer) {
			((EntityPlayer)entity).openGui(TUN.INSTANCE, 0, world, x, y, z);
		}
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister register) {
		this.blockIcon = register.registerIcon(Reference.modid + ":teleporter_side");
		this.blockTop = register.registerIcon(Reference.modid + ":teleporter_top_active");
		this.blockBottom = register.registerIcon(Reference.modid + ":teleporter_bottom");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta) {
		return side == 1 ? this.blockTop : side == 0 ? this.blockBottom : this.blockIcon;
	}
	
}
