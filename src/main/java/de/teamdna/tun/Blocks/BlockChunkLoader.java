package de.teamdna.tun.Blocks;

import de.teamdna.tun.Event.WorldEventHandler;
import de.teamdna.tun.TileEntity.TileEntityChunkLoader;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockChunkLoader extends BlockContainer {

	public BlockChunkLoader() {
		super(Material.iron);
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityChunkLoader();
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		WorldEventHandler.removeChunkLoader(world.getTileEntity(x, y, z));
        super.breakBlock(world, x, y, z, block, meta);
    }
	
	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
        super.onBlockAdded(world, x, y, z);
		WorldEventHandler.addChunkLoader(world.getTileEntity(x, y, z));
    }
	
}
