package de.teamdna.tun.Blocks;

import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.Util.Util;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.util.ForgeDirection;

public abstract class BlockCore extends BlockContainer {

	public BlockCore() {
		super(Material.iron);
		this.setStepSound(Block.soundTypePiston);
	}

	@Override
	public abstract TileEntity createNewTileEntity(World var1, int var2);
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof INetworkComponent && ((INetworkComponent)tile).isLinked()) {
			Object[] uid = Util.getBlockFromUID(((INetworkComponent)tile).getLink());
			World uid_world = DimensionManager.getWorld(Integer.parseInt(String.valueOf(uid[0])));
			int uid_x = Integer.parseInt(String.valueOf(uid[1]));
			int uid_y = Integer.parseInt(String.valueOf(uid[2]));
			int uid_z = Integer.parseInt(String.valueOf(uid[3]));
			
			if(uid_world.getTileEntity(uid_x, uid_y, uid_z) != null && uid_world.getTileEntity(uid_x, uid_y, uid_z) instanceof TileEntityNetworkManager) {
				((TileEntityNetworkManager)uid_world.getTileEntity(uid_x, uid_y, uid_z)).unregisterComponent(Util.createBlockUID(world, x, y, z));
			}
		}
		
        super.breakBlock(world, x, y, z, block, meta);
    }
	
	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
        super.onBlockAdded(world, x, y, z);
        
        INetworkComponent originalTile = (INetworkComponent)world.getTileEntity(x, y, z);
        for(ForgeDirection dir : ForgeDirection.VALID_DIRECTIONS) {
        	TileEntity tile = world.getTileEntity(x + dir.offsetX, y + dir.offsetY, z + dir.offsetZ);
        	if(tile != null && tile instanceof INetworkComponent) {
        		INetworkComponent component = (INetworkComponent)tile;
        		if(component.isLinked()) {
        			originalTile.setLink(component.getLink());
        			
        			Object[] uid = Util.getBlockFromUID(component.getLink());
        			World uid_world = DimensionManager.getWorld(Integer.parseInt(String.valueOf(uid[0])));
        			int uid_x = Integer.parseInt(String.valueOf(uid[1]));
        			int uid_y = Integer.parseInt(String.valueOf(uid[2]));
        			int uid_z = Integer.parseInt(String.valueOf(uid[3]));
        			
        			if(uid_world.getTileEntity(uid_x, uid_y, uid_z) != null && uid_world.getTileEntity(uid_x, uid_y, uid_z) instanceof TileEntityNetworkManager) {
        				((TileEntityNetworkManager)uid_world.getTileEntity(uid_x, uid_y, uid_z)).registerComponent(Util.createBlockUID(world, x, y, z));
        			}
        			break;
        		}
        	} else if(tile != null && tile instanceof TileEntityNetworkManager) {
        		TileEntityNetworkManager manager = (TileEntityNetworkManager)tile;
        		if(!originalTile.isLinked()) {
        			originalTile.setLink(manager.getLink());
        		} else {
        			manager.notifyControllerDetection(world, x, y, z);
        		}
        	}
        }
    }
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int meta, float hitX, float hitY, float hitZ) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof INetworkComponent && ((INetworkComponent)tile).isLinked()) {
			player.addChatMessage(new ChatComponentText(((INetworkComponent)tile).getLink()));
		}
        return false;
    }
	
}
