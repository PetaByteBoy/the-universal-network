package de.teamdna.tun.Event;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.WorldServer;
import net.minecraftforge.event.world.WorldEvent;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.Util.WorldBlock;

public class WorldEventHandler {
	
	public static List<String> chunkLoader = new ArrayList<String>();
	public static List<String> networkManagers = new ArrayList<String>();
	public static List<String> buildingManagers = new ArrayList<String>();
	
	public static void addChunkLoader(TileEntity tile) {
		WorldBlock block = new WorldBlock(tile);
		if(!chunkLoader.contains(block.getBlockUID())) {
			chunkLoader.add(block.getBlockUID());
		}
	}
	
	public static void removeChunkLoader(TileEntity tile) {
		WorldBlock block = new WorldBlock(tile);
		if(chunkLoader.contains(block.getBlockUID())) {
			chunkLoader.remove(block.getBlockUID());
		}
	}
	
	public static void requestManagerRebuilds() {
		for(String s : networkManagers) {
			WorldBlock block = new WorldBlock(s);
			if(block.hasTile() && block.tile instanceof TileEntityNetworkManager && !buildingManagers.contains(block.getBlockUID())) {
				((TileEntityNetworkManager)block.tile).recreateNetwork();
			}
		}
	}
	
	public static void startedBuilding(TileEntity tile) {
		WorldBlock block = new WorldBlock(tile);
		if(!buildingManagers.contains(block.getBlockUID())) {
			buildingManagers.add(block.getBlockUID());
		}
	}
	
	public static void finishedBuilding(TileEntity tile) {
		WorldBlock block = new WorldBlock(tile);
		if(buildingManagers.contains(block.getBlockUID())) {
			buildingManagers.remove(block.getBlockUID());
		}
	}
	
	@SubscribeEvent
	public void eventWorldLoad(WorldEvent.Load event) {
		if(FMLCommonHandler.instance().getEffectiveSide().isServer() && event.world.provider.dimensionId == 0) {
			WorldServer world = (WorldServer)event.world;
			
			try {
				File file = new File(world.getChunkSaveLocation(), "chunks.tun");
				if(file.exists()) {
					BufferedReader buff = new BufferedReader(new FileReader(file));
					for(String s : buff.readLine().split("%")) {
						chunkLoader.add(s);
					}
					buff.close();
				}
			} catch(Exception e) {
			}
		}
	}
	
	@SubscribeEvent
	public void eventWorldSave(WorldEvent.Save event) {
		if(FMLCommonHandler.instance().getEffectiveSide().isServer() && event.world.provider.dimensionId == 0) {
			WorldServer world = (WorldServer)event.world;
			
			try {
				if(world.getChunkSaveLocation().exists()) {
					File file = new File(world.getChunkSaveLocation(), "chunks.tun");
					if(file.exists()) file.delete();
					
					BufferedWriter buff = new BufferedWriter(new FileWriter(file));
					String output = "";
					for(String s : chunkLoader) output += s + "%";
					buff.write(output);
					buff.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
				System.out.println("[TUN] Error by saving chunk data! Chunkloaders and Network Managers have to be loaded manually!");
			}
		}
	}
	
	@SubscribeEvent
	public void eventWorldUnload(WorldEvent.Unload event) {
		if(FMLCommonHandler.instance().getEffectiveSide().isServer() && event.world.provider.dimensionId == 0) {
			TickEventHandler.interrupt = true;
			TickEventHandler.tickCount = 0;
			chunkLoader.clear();
			networkManagers.clear();
			buildingManagers.clear();
		}
	}
	
}
