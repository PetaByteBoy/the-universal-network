package de.teamdna.tun.Event;

import java.util.logging.Level;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.WorldTickEvent;
import cpw.mods.fml.relauncher.Side;
import de.teamdna.tun.TUN;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.Util.WorldBlock;

public class TickEventHandler {

	public static boolean interrupt = true;
	public static int tickCount = 0;
	
	@SubscribeEvent
	public void eventWorldTick(WorldTickEvent event) {
		if(!interrupt && event.side == Side.SERVER) {
			for(String s : WorldEventHandler.chunkLoader) {
				if(s.trim().length() == 0) continue;
				WorldBlock block = new WorldBlock(s);
				block.updateWorld().getChunkProvider().loadChunk(block.getChunkX(), block.getChunkZ());
				
				if(block.hasTile() && block.tile instanceof TileEntityNetworkManager) {
					WorldEventHandler.networkManagers.add(s);
				}
			}
			
			interrupt = true;
		}
		
		if(interrupt && tickCount <= 4) {
			if(++tickCount > 4) {
				interrupt = false;
			}
		}
	}
	
}
