package de.teamdna.tun.Items;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import de.teamdna.tun.Blocks.BlockRemoteInterface;
import de.teamdna.tun.Network.IChunkLoader;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.TileEntity.TileEntityRemoteInterface;
import de.teamdna.tun.Util.Reference;
import de.teamdna.tun.Util.Util;
import de.teamdna.tun.Util.WorldBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFurnace;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemLinkCard extends Item {

	@SideOnly(Side.CLIENT)
	private IIcon itemActive;
	
	public ItemLinkCard() {
		this.maxStackSize = 1;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		if(par1ItemStack.getTagCompound() != null && par1ItemStack.getTagCompound().hasKey("link")) {
			WorldBlock block = new WorldBlock(par1ItemStack.getTagCompound().getString("link"));
			par3List.add(String.valueOf("DIM: " + block.world.provider.dimensionId + " X: " + block.x + " Y: " + block.y + " Z: " + block.z));
		}
	}
	
	@Override
    public String getItemStackDisplayName(ItemStack par1ItemStack) {
		if(par1ItemStack.getTagCompound() != null && par1ItemStack.getTagCompound().hasKey("link")) return StatCollector.translateToLocal("item.tunLinkCard.linked.name");
		else return StatCollector.translateToLocal("item.tunLinkCard.name");
        
    }
	
	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
		MovingObjectPosition hit = getMovingObjectPositionFromPlayer(world, player, true);
		if(!world.isRemote) {
			if(hit == null && player.isSneaking()) {
				stack.setTagCompound(null);
			} else if(hit != null) {
				WorldBlock block = new WorldBlock(world, hit.blockX, hit.blockY, hit.blockZ);
				if(block.hasTile()) {
					if(!(block.tile instanceof INetworkComponent) && !(block.tile instanceof IChunkLoader)) {
						NBTTagCompound tag = new NBTTagCompound();
						tag.setString("link", block.getBlockUID());
						stack.setTagCompound(tag);
						stack.setItemDamage(1);
					} else if(block.tile instanceof TileEntityRemoteInterface && ((INetworkComponent)block.tile).isLinked() && stack.hasTagCompound() && stack.getTagCompound().hasKey("link")) {
						((TileEntityRemoteInterface)block.tile).linkToBlock(stack.getTagCompound().getString("link"));
						BlockRemoteInterface.updateBlockState(world, block.x, block.y, block.z);
						stack.setTagCompound(null);
						stack.setItemDamage(0);
					}
				}
			}
		}
		
        return stack;
    }
	
	@Override
	@SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister par1IconRegister) {
        this.itemIcon = par1IconRegister.registerIcon(Reference.modid + ":" + "link_card");
        this.itemActive = par1IconRegister.registerIcon(Reference.modid + ":" + "link_card_linked");
    }
	
	@Override
	@SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int par1) {
        return par1 == 0 ? this.itemIcon : this.itemActive;
    }
	
}
