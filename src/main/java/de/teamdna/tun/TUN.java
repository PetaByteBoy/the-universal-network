package de.teamdna.tun;

import java.util.List;
import java.util.logging.Logger;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.OrderedLoadingCallback;
import net.minecraftforge.common.ForgeChunkManager.Ticket;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.common.util.EnumHelper;

import com.google.common.collect.Lists;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import de.teamdna.tun.Blocks.BlockAntenna;
import de.teamdna.tun.Blocks.BlockCable;
import de.teamdna.tun.Blocks.BlockChunkLoader;
import de.teamdna.tun.Blocks.BlockNetworkManager;
import de.teamdna.tun.Blocks.BlockRemoteInterface;
import de.teamdna.tun.Blocks.BlockTeleporter;
import de.teamdna.tun.Event.TickEventHandler;
import de.teamdna.tun.Event.WorldEventHandler;
import de.teamdna.tun.GUI.GUIHandler;
import de.teamdna.tun.Items.ItemLinkCard;
import de.teamdna.tun.Net.CommonProxy;
import de.teamdna.tun.Net.PacketPipeline;
import de.teamdna.tun.Net.Packet.PacketLinkSync;
import de.teamdna.tun.Net.Packet.PacketTeleportRequest;
import de.teamdna.tun.Net.Packet.PacketTeleporterSync;
import de.teamdna.tun.Network.IChunkLoader;
import de.teamdna.tun.TileEntity.TileEntityAntenna;
import de.teamdna.tun.TileEntity.TileEntityCable;
import de.teamdna.tun.TileEntity.TileEntityChunkLoader;
import de.teamdna.tun.TileEntity.TileEntityCore;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.TileEntity.TileEntityRemoteInterface;
import de.teamdna.tun.TileEntity.TileEntityTeleporter;
import de.teamdna.tun.Util.Reference;

@Mod(modid = Reference.modid, name = Reference.name, version = Reference.version)
public class TUN {

	public static final Logger logger = Logger.getLogger("TUN");
	public static final PacketPipeline packetPipeline = new PacketPipeline();
	
	@Instance(Reference.modid)
	public static TUN INSTANCE;
	
	@SidedProxy(clientSide = Reference.clientProxy, serverSide = Reference.commonProxy)
	public static CommonProxy proxy;
	
	public static CreativeTabs cTab = new CreativeTabs(CreativeTabs.getNextID(), "tun") {
		public Item getTabIconItem() { return (new ItemStack(TUN.networkManager)).getItem(); }
    };
	
	public static Block networkManager;
	public static Block cable;
	public static Block teleporter;
	public static Block antenna;
	public static Block chunkLoader;
	public static Block remoteInterface;
	
	public static Item linkCard;
	
	public static boolean allowChunkLoaderBlock = true;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		Configuration c = new Configuration(event.getSuggestedConfigurationFile());
		c.load();
		
		Property p1 = c.get(Configuration.CATEGORY_GENERAL, "BOOL.AllowChunkLoader", allowChunkLoaderBlock);
		p1.comment = "Decide whether Chunk Loader Blocks are allowed or not";
		allowChunkLoaderBlock = p1.getBoolean(allowChunkLoaderBlock);
		
		c.save();

		networkManager  = (new BlockNetworkManager()).setCreativeTab(cTab).setBlockName("tunNetworkManager").setBlockTextureName("obsidian");
		cable		    = (new BlockCable()).setCreativeTab(cTab).setBlockName("tunCable");
		teleporter 	    = (new BlockTeleporter()).setCreativeTab(cTab).setBlockName("tunTeleporter");
		antenna		    = (new BlockAntenna()).setCreativeTab(cTab).setBlockName("tunAntenna").setBlockTextureName("dirt");
		chunkLoader	    = (new BlockChunkLoader()).setCreativeTab(cTab).setBlockName("tunChunkLoader").setBlockTextureName("stone");
		remoteInterface = (new BlockRemoteInterface()).setCreativeTab(cTab).setBlockName("tunRemoteInterface");

		linkCard 	    = (new ItemLinkCard()).setCreativeTab(cTab).setUnlocalizedName("tunLinkCard");
		
		proxy.registerBlock(networkManager);
		proxy.registerBlock(cable);
		proxy.registerBlock(teleporter);
		proxy.registerBlock(antenna);
		if(allowChunkLoaderBlock) proxy.registerBlock(chunkLoader);
		proxy.registerBlock(remoteInterface);
		
		proxy.registerItem(linkCard);
		
		proxy.registerTileEntity(TileEntityCore.class);
		proxy.registerTileEntity(TileEntityNetworkManager.class);
		proxy.registerTileEntity(TileEntityCable.class);
		proxy.registerTileEntity(TileEntityTeleporter.class);
		proxy.registerTileEntity(TileEntityAntenna.class);
		if(allowChunkLoaderBlock) proxy.registerTileEntity(TileEntityChunkLoader.class);
		proxy.registerTileEntity(TileEntityRemoteInterface.class);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		packetPipeline.initalise();
		packetPipeline.registerPacket(PacketTeleporterSync.class);
		packetPipeline.registerPacket(PacketLinkSync.class);
		packetPipeline.registerPacket(PacketTeleportRequest.class);
		
		proxy.registerRenderer();
		
		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GUIHandler());
		
		MinecraftForge.EVENT_BUS.register(new WorldEventHandler());
		FMLCommonHandler.instance().bus().register(new TickEventHandler());
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		packetPipeline.postInitialise();
		
		try {
			proxy.loadFonts();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ForgeChunkManager.setForcedChunkLoadingCallback(TUN.INSTANCE, new OrderedLoadingCallback() {
			@Override
			public void ticketsLoaded(List<Ticket> tickets, World world) {
				for(Ticket t : tickets) {
					int x = t.getModData().getInteger("bx");
					int y = t.getModData().getInteger("by");
					int z = t.getModData().getInteger("bz");
					IChunkLoader loader = (IChunkLoader)world.getTileEntity(x, y, z);
					loader.loadChunks(t);
				}
			}

			@Override
			public List<Ticket> ticketsLoaded(List<Ticket> tickets, World world, int maxTicketCount) {
				List<Ticket> valids = Lists.newArrayList();
				for(Ticket t : tickets) {
					int x = t.getModData().getInteger("bx");
					int y = t.getModData().getInteger("by");
					int z = t.getModData().getInteger("bz");
					
					TileEntity tile = world.getTileEntity(x, y, z);
					if(tile != null && tile instanceof IChunkLoader) {
						valids.add(t);
					}
				}
				return valids;
			}
		});
	}
	
}
