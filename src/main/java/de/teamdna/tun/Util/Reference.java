package de.teamdna.tun.Util;

public class Reference {

	public static final String modid   = "tun";
	public static final String name    = "TUN - The Universal Network";
	public static final String version = "0.0.0";
	
	public static final String clientProxy = "de.teamdna.tun.Net.ClientProxy";
	public static final String commonProxy = "de.teamdna.tun.Net.CommonProxy";
	
}
