package de.teamdna.tun.Util;

import net.minecraft.util.AxisAlignedBB;

public class AABBHelper {

	/**
	 * Translates the position of an AxisAlignedBoundingBox
	 * @param original The AABB to translate
	 * @param orignal_x The x position of the BB
	 * @param orignal_y The y position of the BB
	 * @param orignal_z The z position of the BB
	 * @param x The new x position of the BB
	 * @param y The new y position of the BB
	 * @param z The new z position of the BB
	 */
	public static AxisAlignedBB translateEntityAABB(AxisAlignedBB original, double orignal_x, double orignal_y, double orignal_z, double x, double y, double z) {
		AxisAlignedBB aabb = original.copy();
		aabb.minX -= orignal_x;
		aabb.minY -= orignal_y;
		aabb.minZ -= orignal_z;
		aabb.maxX -= orignal_x;
		aabb.maxY -= orignal_y;
		aabb.maxZ -= orignal_z;
		aabb.minX += x;
		aabb.minY += y;
		aabb.minZ += z;
		aabb.maxX += x;
		aabb.maxY += y;
		aabb.maxZ += z;
		
		return aabb;
	}
	
}
