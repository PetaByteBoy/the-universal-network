package de.teamdna.tun.Util;

import net.minecraft.world.World;

public class Util {

	/**
	 * Returns a unique ID for a block in a world
	 */
	public static String createBlockUID(World world, int x, int y, int z) {
		return String.valueOf(world.provider.dimensionId + "/" + x + "/" + y + "/" + z);
	}
	
	/**
	 * Returns an Object-Array of the parameters of Util.createBlockUID
	 */
	public static Object[] getBlockFromUID(String uid) {
		return uid.split("/");
	}
	
}
