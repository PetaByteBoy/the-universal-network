package de.teamdna.tun.Util;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.*;
import org.newdawn.slick.opengl.TextureImpl;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class SlickFont {
	
	public Font awtFont;
	public TrueTypeFont font;
	
	public double detail = 6D;
	
	public SlickFont(ResourceLocation rl, float size) throws IOException, FontFormatException {
		InputStream stream = Minecraft.getMinecraft().getResourceManager().getResource(rl).getInputStream();
		this.awtFont = Font.createFont(Font.TRUETYPE_FONT, stream);
		this.font = new TrueTypeFont(this.awtFont.deriveFont((float)(this.detail * 8)), true);
	}
	
	public float getStringWidth(String s) {
		return this.font.getWidth(s);
	}
	
	public void drawString(float x, float y, String s, Color color, float size) {
		TextureImpl.bindNone();
		GL11.glPushMatrix();
		GL11.glScalef(size, size, size);
		this.font.drawString(x, y, s, color);
		GL11.glPopMatrix();
	}
	
}
