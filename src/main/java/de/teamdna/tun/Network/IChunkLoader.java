package de.teamdna.tun.Network;

import net.minecraftforge.common.ForgeChunkManager.Ticket;

public interface IChunkLoader {

	void loadChunks(Ticket ticket);
	
}
