package de.teamdna.tun.Network;

public interface INetworkComponent {
	
	/**
	 * Returns whether the Tile is linked to a controller
	 */
	boolean isLinked();
	
	/**
	 * Links the tile to a controller
	 */
	void setLink(String link);
	
	/**
	 * Returns the link to a controller
	 */
	String getLink();
	
}
