package de.teamdna.tun.Net;

import java.awt.FontFormatException;
import java.io.IOException;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.oredict.ShapedOreRecipe;
import cpw.mods.fml.common.registry.GameRegistry;
import de.teamdna.tun.Util.Reference;

public class CommonProxy {

	public void registerBlock(Block block) {
		GameRegistry.registerBlock(block, Reference.modid + ":" + block.getUnlocalizedName().toLowerCase());
	}
	
	public void registerItem(Item item) {
		GameRegistry.registerItem(item, Reference.modid + ":" + item.getUnlocalizedName().toLowerCase());
	}
	
	public void registerTileEntity(Class <?extends TileEntity> tileEntityClass) {
		GameRegistry.registerTileEntity(tileEntityClass, Reference.modid + ":" + tileEntityClass.getName().toLowerCase());
	}
	
	public void addShapedRecepie(ItemStack output, Object... params) {
		GameRegistry.addShapedRecipe(output, params);
	}
	
	public void addShapedOreRecipe(ItemStack result, Object... recepie) {
		GameRegistry.addRecipe(new ShapedOreRecipe(result, recepie));
	}
	
	public void registerRenderer() {  }
	
	public void loadFonts() throws IOException, FontFormatException {  }
	
}
