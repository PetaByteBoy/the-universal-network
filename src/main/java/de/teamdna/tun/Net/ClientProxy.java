package de.teamdna.tun.Net;

import java.awt.FontFormatException;
import java.io.IOException;

import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import de.teamdna.tun.Renderer.RenderBlockCable;
import de.teamdna.tun.Renderer.RenderBlockRemoteInterface;
import de.teamdna.tun.Renderer.RenderTileRemoteInterface;
import de.teamdna.tun.TileEntity.TileEntityRemoteInterface;
import de.teamdna.tun.Util.Reference;
import de.teamdna.tun.Util.SlickFont;

public class ClientProxy extends CommonProxy {

	public static int renderBlockIOCableID;
	public static int renderBlockRemoteInterfaceID;
	
	public static SlickFont customFont1;
	
	@Override
	public void registerRenderer() {
		renderBlockIOCableID = RenderingRegistry.getNextAvailableRenderId();
		RenderingRegistry.registerBlockHandler(renderBlockIOCableID, new RenderBlockCable());
		
		renderBlockRemoteInterfaceID = RenderingRegistry.getNextAvailableRenderId();
		RenderingRegistry.registerBlockHandler(renderBlockRemoteInterfaceID, new RenderBlockRemoteInterface());
		
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityRemoteInterface.class, new RenderTileRemoteInterface());
//		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCore.class, new RenderTileCore());
	}
	
	@Override
	public void loadFonts() throws IOException, FontFormatException {
		customFont1 = new SlickFont(new ResourceLocation(Reference.modid, "fonts/Wallpoet-Regular.ttf"), 32);
	}
	
}
