package de.teamdna.tun.Net.Packet;

import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;


public abstract class AbstractPacket {

    public abstract void encodeInto(ChannelHandlerContext ctx, PacketBuffer buffer);

    public abstract void decodeInto(ChannelHandlerContext ctx, PacketBuffer buffer);

    public abstract void handleClientSide(EntityPlayer player);

    public abstract void handleServerSide(EntityPlayer player);
    
}