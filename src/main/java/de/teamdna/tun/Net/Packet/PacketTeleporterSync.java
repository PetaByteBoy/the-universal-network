package de.teamdna.tun.Net.Packet;

import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import de.teamdna.tun.TileEntity.TileEntityTeleporter;
import de.teamdna.tun.Util.WorldBlock;

public class PacketTeleporterSync extends AbstractPacket {

	private WorldBlock block;
	private String name;
	
	public PacketTeleporterSync() {
	}
	
	public PacketTeleporterSync(World world, int x, int y, int z, String name) {
		this.block = new WorldBlock(world, x, y, z);
		this.name = name;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, PacketBuffer buffer) {
		buffer.writeInt(this.block.world.provider.dimensionId);
		buffer.writeInt(this.block.x);
		buffer.writeInt(this.block.y);
		buffer.writeInt(this.block.z);
		buffer.writeInt(this.name.getBytes().length);
		buffer.writeBytes(this.name.getBytes());
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, PacketBuffer buffer) {
		this.block = new WorldBlock(DimensionManager.getWorld(buffer.readInt()), buffer.readInt(), buffer.readInt(), buffer.readInt());
		int size = buffer.readInt();
		this.name = new String(buffer.readBytes(size).array());
	}

	@Override
	public void handleClientSide(EntityPlayer player) {
	}

	@Override
	public void handleServerSide(EntityPlayer player) {
		((TileEntityTeleporter)this.block.updateTileEntity()).name = this.name;
	}

}
