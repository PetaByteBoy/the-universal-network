package de.teamdna.tun.Net.Packet;

import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import cpw.mods.fml.common.FMLCommonHandler;
import de.teamdna.tun.TileEntity.TileEntityNetworkManager;
import de.teamdna.tun.Util.WorldBlock;

public class PacketTeleportRequest extends AbstractPacket {

	public int entityID, dimID;
	public String endPoint, manager;
	
	public PacketTeleportRequest() {
	}
	
	public PacketTeleportRequest(int entityID, int dimID, String endPoint, String manager) {
		this.entityID = entityID;
		this.dimID = dimID;
		this.endPoint = endPoint;
		this.manager = manager;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, PacketBuffer buffer) {
		buffer.writeInt(this.entityID);
		buffer.writeInt(this.dimID);
		buffer.writeInt(this.endPoint.getBytes().length);
		buffer.writeBytes(this.endPoint.getBytes());
		buffer.writeInt(this.manager.getBytes().length);
		buffer.writeBytes(this.manager.getBytes());
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, PacketBuffer buffer) {
		this.entityID = buffer.readInt();
		this.dimID = buffer.readInt();
		int size = buffer.readInt();
		this.endPoint = new String(buffer.readBytes(size).array());
		size = buffer.readInt();
		this.manager = new String(buffer.readBytes(size).array());
	}

	@Override
	public void handleClientSide(EntityPlayer player) {
	}

	@Override
	public void handleServerSide(EntityPlayer player) {
		World world = DimensionManager.getWorld(this.dimID);
		Entity entity = world.getEntityByID(this.entityID);
		
		if(entity != null && entity instanceof EntityPlayer) {
			WorldBlock endPoint = new WorldBlock(this.endPoint);
			WorldBlock manager = new WorldBlock(this.manager);
			
			if(manager.exists() && ((TileEntityNetworkManager)manager.updateTileEntity()).canWork() && endPoint.exists()) {
				EntityPlayerMP playerMP = FMLCommonHandler.instance().getMinecraftServerInstance().getConfigurationManager().getPlayerForUsername(((EntityPlayer)entity).getCommandSenderName());
				
				if(playerMP.dimension != endPoint.world.provider.dimensionId) playerMP.travelToDimension(endPoint.world.provider.dimensionId);
				
				playerMP.mountEntity((Entity)null);
				playerMP.playerNetServerHandler.setPlayerLocation(endPoint.x + 0.5F, endPoint.y + 1, endPoint.z + 0.5F, playerMP.rotationYaw, playerMP.rotationPitch);
				
				playerMP.worldObj.playSoundEffect(player.posX, player.posY, player.posZ, "mob.endermen.portal", 1.0F, 1.0F);
			}
		}
	}

}
