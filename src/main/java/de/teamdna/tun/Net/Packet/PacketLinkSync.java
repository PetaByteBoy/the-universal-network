package de.teamdna.tun.Net.Packet;

import io.netty.channel.ChannelHandlerContext;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import de.teamdna.tun.Network.INetworkComponent;
import de.teamdna.tun.TileEntity.TileEntityRemoteInterface;
import de.teamdna.tun.Util.WorldBlock;

public class PacketLinkSync extends AbstractPacket {

	private WorldBlock block;
	private String link;
	private NBTTagCompound nbtTag;
	
	public PacketLinkSync() {
	}
	
	public PacketLinkSync(World world, int x, int y, int z, String link, NBTTagCompound nbtTag) {
		this.block = new WorldBlock(world, x, y, z);
		this.link = link;
		this.nbtTag = nbtTag;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, PacketBuffer buffer) {
		buffer.writeInt(this.block.world.provider.dimensionId);
		buffer.writeInt(this.block.x);
		buffer.writeInt(this.block.y);
		buffer.writeInt(this.block.z);
		buffer.writeInt(this.link.getBytes().length);
		buffer.writeBytes(this.link.getBytes());
		try {
			buffer.writeNBTTagCompoundToBuffer(this.nbtTag);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, PacketBuffer buffer) {
		this.block = new WorldBlock(DimensionManager.getWorld(buffer.readInt()), buffer.readInt(), buffer.readInt(), buffer.readInt());
		int size = buffer.readInt();
		this.link = new String(buffer.readBytes(size).array());
		try {
			this.nbtTag = buffer.readNBTTagCompoundFromBuffer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleClientSide(EntityPlayer player) {
		this.block.world = player.worldObj;
		if(this.block.updateTileEntity() != null) {
			((INetworkComponent)this.block.tile).setLink(this.link);
			
			if(this.block.tile instanceof TileEntityRemoteInterface) {
				this.block.tile.readFromNBT(this.nbtTag);
				Minecraft.getMinecraft().renderGlobal.markBlockForRenderUpdate(this.block.x, this.block.y, this.block.z);
			}
		}
	}

	@Override
	public void handleServerSide(EntityPlayer player) {
	}

}
